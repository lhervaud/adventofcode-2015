# How many total square feet of wrapping paper should they order?

# PART 2
# How many total feet of ribbon should they order?

from sys import stdin, argv

PART = int(argv[1])

def main():

    dimensions = []
    # init puzzle
    for line in stdin:
        input = line.rstrip()
        box = input.split('x')        
        box[0] = int(box[0])
        box[1] = int(box[1])
        box[2] = int(box[2])
        box.sort()
        dimensions.append(box)

    if PART == 1:
        print("square feet:",run_part1(dimensions))
    else:
        print("feet:",run_part2(dimensions))

def run_part1(dimensions):

    square_feet = 0
    for box in dimensions:
        sf = (2*box[0]*box[1]) + (2*box[0]*box[2]) + (2*box[1]*box[2]) + min(box[0]*box[1],box[0]*box[2],box[1]*box[2])
        square_feet += sf
    return(square_feet)

def run_part2(dimensions):

    feet = 0
    for box in dimensions:
        f = (2*box[0]) + (2*box[1]) + (box[0]*box[1]*box[2])
        feet += f
    return(feet)

if __name__ == "__main__":
    main()
