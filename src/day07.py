# what signal is ultimately provided to wire a?

# PART 2
# 

from sys import stdin, argv
import re

PART = int(argv[1])

def main():

    compute = Compute()
    instructions = Instructions()
    compute2 = Compute()
    instructions2 = Instructions()
    # init puzzle
    for line in stdin:
        input = line.rstrip()
        instructions.add(instruction = input)
        instructions2.add(instruction = input)

    if PART == 1:
        print("a:", compute.apply_instructions(instructions))
        print(compute.dict)
    else:
        a = compute.apply_instructions(instructions)
        print(compute.dict)
        instructions2.remove_destination("b")
        instructions2.add(instruction = str(a) + " -> b")
        print("a:", compute2.apply_instructions(instructions2))

class Compute:

    def __init__(self):
        self.dict = {}

    def apply_instructions(self, instructions):
        while "a" not in self.dict:
            for i in instructions.list:
                #print(i)
                if i.computed == False:
                    if i.operator == "":
                        if i.value2 != None:
                            self.dict[i.destination] = i.value2
                            i.computed = True
                        else:
                            if i.var2 in self.dict:
                                self.dict[i.destination] = self.dict[i.var2]
                                i.computed = True
                    else:
                        if i.value1 == None:
                            if i.var1 in self.dict:
                                i.value1 = self.dict[i.var1]
                        if i.value2 == None:
                            if i.var2 in self.dict:
                                i.value2 = self.dict[i.var2]
                        if i.operator == "NOT" and i.value2 != None:
                            self.dict[i.destination] = ~ i.value2 + 65536
                            i.computed = True
                        if i.value1 != None and i.value2 != None:
                            if i.operator == "AND":
                                self.dict[i.destination] = i.value1 & i.value2
                                i.computed = True
                            elif i.operator == "OR":
                                self.dict[i.destination] = i.value1 | i.value2
                                i.computed = True
                            elif i.operator == "LSHIFT":
                                self.dict[i.destination] = i.value1 << i.value2
                                i.computed = True
                            elif i.operator == "RSHIFT":
                                self.dict[i.destination] = i.value1 >> i.value2
                                i.computed = True
                            else:
                                print("unknown operator:", i.operator)

        return(self.dict["a"])

class Instructions:

    def __init__(self):
        self.list = []

    def add(self, instruction):
        operator = ""
        value1 = None
        var1 = None
        value2 = None
        var2 = None
        destination = None
        result = re.search(r"([a-z0-9]*)\s?([A-Z]+)\s([a-z0-9]+) -> ([a-z]+)", instruction)
        if result:
            operator = result.group(2)
            if re.match("\d+", result.group(1)):
                value1 = int(result.group(1))
            else:
                var1 = result.group(1)
            if re.match("\d+", result.group(3)):
                value2 = int(result.group(3))
            else:
                var2 = result.group(3)
            destination = result.group(4)
            self.list.append(Instruction(operator, value1, var1, value2, var2, destination))
        else:
            result = re.search(r"([a-z0-9]+) -> ([a-z]+)", instruction)
            if result:
                if re.match("\d+", result.group(1)):
                    value2 = int(result.group(1))
                else:
                    var2 = result.group(1)
                destination = result.group(2)
                self.list.append(Instruction(operator, value1, var1, value2, var2, destination))
            else:
                print("cannot add:", instruction)

    def remove_destination(self, destination):
        for i in self.list:
            if i.destination == destination:
                self.list.remove(i)

class Instruction:

    def __init__(self, operator, value1, var1, value2, var2, destination):
        self.operator = operator
        self.value1 = value1
        self.var1 = var1
        self.value2 = value2
        self.var2 = var2
        self.destination = destination
        self.computed = False

    def __str__(self):
        return("operator:"+str(self.operator)+", value1:"+str(self.value1)+", var1:"+str(self.var1)+", value2:"+str(self.value2)+", var2:"+str(self.var2)+", destination:"+str(self.destination)+", computed:"+str(self.computed))

if __name__ == "__main__":
    main()
