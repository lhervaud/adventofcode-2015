# How many distinct molecules can be created after all the different ways you can do one replacement on the medicine molecule?

# PART 2
# what is the fewest number of steps to go from e to the medicine molecule?

from sys import stdin, argv
from time import time
import re

PART = int(argv[1])

def main():

    molecules = Molecules()
    # init puzzle
    for line in stdin:
        molecules.add_parse(line.rstrip())

    t = time()

    if PART == 1:
        print(len(molecules.apply_replacements()))
    else:
        print(len(molecules.reverse_replacements()))

    print('time:', time()-t)

class Molecules():

    def __init__(self):
        self.replacements = []
        self.start_molecule = ""

    def add_parse(self, line):
        result = re.search(r"([a-zA-Z]+) => ([a-zA-Z]+)", line)
        if result:
            t = (result.group(1), result.group(2))
            if t[0] == "e":
                t = ("e", "^" + t[1] + "$")
            self.replacements.append(t)
        else:
            if line:
                self.start_molecule = line

    def apply_replacements(self):
        list_generated = []

        for r in self.replacements:
            for m in re.finditer(r[0], self.start_molecule):
                molecule_generated = self.start_molecule[:m.start()] + r[1] + self.start_molecule[m.end():]
                list_generated.append(molecule_generated)
                print(molecule_generated)

        return set(list_generated)
    
    def reverse_replacements(self):
        list_generated = []
        reversed_molecule = self.start_molecule

        while reversed_molecule != "e":
            for r in self.replacements:
                new_molecule = re.sub(r[1], r[0], reversed_molecule, 1)
                while new_molecule != reversed_molecule:
                    reversed_molecule = new_molecule
                    print(reversed_molecule)
                    list_generated.append(reversed_molecule)
                    new_molecule = re.sub(r[1], r[0], reversed_molecule, 1)


        return set(list_generated)



if __name__ == "__main__":
    main()
