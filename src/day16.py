# What is the number of the Sue that got you the gift?

# PART 2
# What is the number of the real Aunt Sue?

from sys import stdin, argv
from time import time
import re

PART = int(argv[1])
MFCSAM = {"children": 3, "cats": 7, "samoyeds": 2, "pomeranians": 3, "akitas": 0, "vizslas": 0, "goldfish": 5, "trees": 3, "cars": 2, "perfumes": 1}

def main():

    sues = Sues()
    # init puzzle
    for line in stdin:
        sues.add_parse(line.rstrip())

    t = time()

    print("matches:", sues.get_matches(MFCSAM))

    print('time:', time()-t)

class Sues():

    def __init__(self):
        self.list = []

    def add_parse(self, line):
        result = re.search(r"Sue (\d+): ([a-zA-Z]+): (\d+), ([a-zA-Z]+): (\d+), ([a-zA-Z]+): (\d+)", line)
        if result:
            s = Sue("Sue " + result.group(1))
            for i in range(2, 7, 2):
                s.mfcsam[result.group(i)] = int(result.group(i+1))            
            self.list.append(s)
        else:
            print("cannot add:", line)

    def get_matches(self, mfcsam):
        matches = []
        for s in self.list:
            match = True
            for k in mfcsam.keys():
                if k in s.mfcsam.keys():
                    if PART == 1:
                        if mfcsam[k] != s.mfcsam[k]:
                            match = False
                            break
                    elif PART == 2:
                        if (k == "cats" or k == "trees") and mfcsam[k] < s.mfcsam[k]:
                            match = True
                        elif (k == "pomeranians" or k == "goldfish") and mfcsam[k] > s.mfcsam[k]:
                            match = True
                        elif not (k == "cats" or k == "trees" or k == "pomeranians" or k == "goldfish") and mfcsam[k] == s.mfcsam[k]:
                            match = True
                        else:
                            match = False
                            break
            if match:
                matches.append(s.name)

        return matches

class Sue:
    def __init__(self, name):
        self.name = name
        self.mfcsam = {}


if __name__ == "__main__":
    main()
