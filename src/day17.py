# how many different combinations of containers can exactly fit all 150 liters of eggnog?

# PART 2
# 

from sys import stdin, argv
from time import time
from itertools import combinations

PART = int(argv[1])
NB_LITERS = 150

def main():

    containers = Containers()
    # init puzzle
    for line in stdin:
        containers.list.append(int(line.rstrip()))
    containers.combine()

    t = time()

    f_combi = containers.filter_combi(NB_LITERS)

    if PART == 1:
        #print(f_combi)
        print("combinations :", len(f_combi))
    else:
        nb = 0
        list_len = 0
        for l in f_combi:
            if nb == 0:
                nb = 1
                list_len = len(l)
            else:
                if len(l) == list_len:
                    nb += 1
                else:
                    break
        print("combinations of %s: %s" % (list_len, nb))

    print('time:', time()-t)

class Containers():

    def __init__(self):
        self.list = []
        self.combi = []

    def combine(self):
        for i in range(1, len(self.list)+1):
            self.combi += combinations(self.list, i)

    def filter_combi(self, liters):
        filtered_combi = []
        for c in self.combi:
            if sum(c) == liters:
                filtered_combi.append(c)
        
        return(filtered_combi)


if __name__ == "__main__":
    main()
