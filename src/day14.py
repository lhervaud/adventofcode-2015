# what distance has the winning reindeer traveled?

# PART 2
# how many points does the winning reindeer have?

from sys import stdin, argv
from time import time
import re
from itertools import permutations

PART = int(argv[1])
POS_SEC = 2503
#POS_SEC = 1000

def main():

    reind = Reindeers()
    # init puzzle
    for line in stdin:
        reind.add_parse(line.rstrip())

    t = time()

    if PART == 1:
        print("max_distance:", reind.get_max_distance(POS_SEC))
    else:
        print("max_point:", reind.get_max_point(POS_SEC))

    print('time:', time()-t)

class Reindeers():

    def __init__(self):
        self.list = []

    def add_parse(self, line):
        result = re.search(r"([a-zA-Z]+) can fly (\d+) km/s for (\d+) seconds, but then must rest for (\d+) seconds.", line)
        if result:
            self.list.append(Reindeer(result.group(1), int(result.group(2)), int(result.group(3)), int(result.group(4))))
        else:
            print("cannot add:", line)

    def get_max_distance(self, duration):

        distance_max = 0
        for r in self.list:
            distance = 0
            round = int(duration / (r.fly_duration + r.rest_duration))
            distance = round * r.fly_duration * r.speed
            rest = duration % (r.fly_duration + r.rest_duration)
            if rest < r.fly_duration:
                distance += rest * r.speed
            else:
                distance += r.fly_duration * r.speed
            if distance > distance_max:
                distance_max = distance

        return(distance_max)

    def get_max_point(self, duration):

        point_max = 0
        distances = {}
        reindeer_point = {}
        for r in self.list:
            distances[r.name] = 0
            reindeer_point[r.name] = 0

        for s in range(1, duration+1):            
            for r in self.list:
                distance = 0
                round = int(s / (r.fly_duration + r.rest_duration))
                distance = round * r.fly_duration * r.speed
                rest = s % (r.fly_duration + r.rest_duration)
                if rest < r.fly_duration:
                    distance += rest * r.speed
                else:
                    distance += r.fly_duration * r.speed
                distances[r.name] = distance
            dist_sorted = sorted(distances.items(), key=lambda x:x[1], reverse=True)
            max_dist = 0
            #print('d:',dist_sorted)
            for dsorted in dist_sorted:
                if dsorted[1] >= max_dist:
                    reindeer_point[dsorted[0]] += 1
                    max_dist = dsorted[1]
                else:
                    break
            #print(reindeer_point)

        point_sorted = sorted(reindeer_point.items(), key=lambda x:x[1], reverse=True)

        return(list(point_sorted)[0])

class Reindeer:
    def __init__(self, name, speed, fly_duration, rest_duration):
    
        self.name = name
        self.speed = speed
        self.fly_duration = fly_duration
        self.rest_duration = rest_duration


if __name__ == "__main__":
    main()
