# What is the total change in happiness for the optimal seating arrangement of the actual guest list?

# PART 2
# 

from sys import stdin, argv
from time import time
import re
from itertools import permutations

PART = int(argv[1])

def main():

    binom = Binomials()
    # init puzzle
    for line in stdin:
        binom.add_parse(line.rstrip())

    t = time()

    if PART == 1:
        comp = Compute(binom)
        print("max_score:", comp.get_max_score(binom))
    else:
        binom.add_me()
        comp = Compute(binom)
        print("max_score:", comp.get_max_score(binom))

    print('time:', time()-t)

class Compute():

    def __init__(self, Binomials):
        list_dedoub = Binomials.get_attendees()
        self.alliter = permutations(list_dedoub, len(list_dedoub))
    
    def get_max_score(self, Binomials):
        score_max = 0
        for group in self.alliter:
            score = 0
            gcount = len(group)
            for i in range(gcount):
                if i == 0:
                    score += Binomials.get_score(group[i], group[gcount-1])
                    score += Binomials.get_score(group[i], group[i+1])
                elif i == gcount-1:
                    score += Binomials.get_score(group[i], group[i-1])
                    score += Binomials.get_score(group[i], group[0])
                else:
                    score += Binomials.get_score(group[i], group[i-1])
                    score += Binomials.get_score(group[i], group[i+1])
            if score > score_max:
                score_max = score
        return(score_max)

class Binomials():

    def __init__(self):
        self.list = []

    def add_parse(self, line):
        result = re.search(r"([a-zA-Z]+) would (gain|lose) (\d+) happiness units by sitting next to ([a-zA-Z]+).", line)
        if result:
            score = int(result.group(3))
            if result.group(2) == "lose":
                score = score * -1
            p1 = result.group(1)
            p2 = result.group(4)
            self.list.append(Binomial(p1, p2, score))
        else:
            print("cannot add:", line)

    def get_attendees(self):
        s = set()
        for b in self.list:
            s.add(b.p1)
        return(list(s))

    def add_me(self):
        attendees = self.get_attendees()
        for a in attendees:
            self.list.append(Binomial(a, "Me", 0))
            self.list.append(Binomial("Me", a, 0))

    def get_score(self, p1, p2):
        for b in self.list:
            if b.p1 == p1 and b.p2 == p2:
                return(b.score)

class Binomial:
    def __init__(self, p1, p2, score):
    
        self.p1 = p1
        self.p2 = p2
        self.score = score


if __name__ == "__main__":
    main()
