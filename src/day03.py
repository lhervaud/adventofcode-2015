# How many houses receive at least one present?

# PART 2
# how many houses receive at least one present?

from sys import stdin, argv

PART = int(argv[1])

def main():

    directions = []
    # init puzzle
    for line in stdin:
        input = line.rstrip()
        directions.append(input)

    if PART == 1:
        print("houses:",run_part1(directions[0]))
    else:
        print("houses:",run_part2(directions[0]))

def run_part1(moves):

    position = (0, 0)
    coords = []
    coords.append(position)
    for move in moves:
        if move == '^':
            position = (position[0],position[1]+1)
        elif move == '>':
            position = (position[0]+1,position[1])
        elif move == 'v':
            position = (position[0],position[1]-1)
        elif move == '<':
            position = (position[0]-1,position[1])
        coords.append(position)
        
    return(len(set(coords)), coords)

def run_part2(moves):

    # split in 2 lists of directions
    moves_santa = []
    moves_robo = []
    i = 0
    for move in moves:
        if i%2 == 0:
            moves_santa.append(move)
        else:
            moves_robo.append(move)
        i += 1
    
    (run_santa, coords_santa) = run_part1(moves_santa)
    (run_robo, coords_robo) = run_part1(moves_robo)

    return(len(set(coords_santa+coords_robo)))

if __name__ == "__main__":
    main()
