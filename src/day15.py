# what is the total score of the highest-scoring cookie you can make?

# PART 2
# what is the total score of the highest-scoring cookie you can make with a calorie total of 500?

from sys import stdin, argv
from time import time
import re
from itertools import permutations

PART = int(argv[1])

def main():

    recipe = Ingredients()
    # init puzzle
    for line in stdin:
        recipe.add_parse(line.rstrip())

    t = time()

    if PART == 1:
        print("max_score:", recipe.get_max_score(0))
    else:
        print("max_score / 500 cal:", recipe.get_max_score(500))

    print('time:', time()-t)

class Ingredients():

    def __init__(self):
        self.list = []

    def add_parse(self, line):
        result = re.search(r"([a-zA-Z]+): capacity (-?\d+), durability (-?\d+), flavor (-?\d+), texture (-?\d+), calories (-?\d+)", line)
        if result:
            self.list.append(Ingredient(result.group(1), int(result.group(2)), int(result.group(3)), int(result.group(4)), int(result.group(5)), int(result.group(6))))
        else:
            print("cannot add:", line)

    def get_max_score(self, calories_wanted):
        max_score = 0
        list_100 = list()
        for r in range(101):
            list_100.append(r)
        alliter = permutations(list_100, 4)
        list_sum = list()
        for a in alliter:
            if sum(a) == 100:
                list_sum.append(a)

        for l in list_sum:
            capacity = l[0]*self.list[0].capacity + l[1]*self.list[1].capacity + l[2]*self.list[2].capacity + l[3]*self.list[3].capacity
            durability = l[0]*self.list[0].durability + l[1]*self.list[1].durability + l[2]*self.list[2].durability + l[3]*self.list[3].durability
            flavor = l[0]*self.list[0].flavor + l[1]*self.list[1].flavor + l[2]*self.list[2].flavor + l[3]*self.list[3].flavor
            texture = l[0]*self.list[0].texture + l[1]*self.list[1].texture + l[2]*self.list[2].texture + l[3]*self.list[3].texture
            calories = l[0]*self.list[0].calories + l[1]*self.list[1].calories + l[2]*self.list[2].calories + l[3]*self.list[3].calories
            score = max(capacity, 0) * max(durability, 0) * max(flavor, 0) * max(texture, 0)
            if score > max_score:
                if calories_wanted > 0:
                    if calories == calories_wanted:
                        max_score = score
                else:
                    max_score = score
        
        return(max_score)

class Ingredient:
    def __init__(self, name, capacity, durability, flavor, texture, calories):
        self.name = name
        self.capacity = capacity
        self.durability = durability
        self.flavor = flavor
        self.texture = texture
        self.calories = calories


if __name__ == "__main__":
    main()
