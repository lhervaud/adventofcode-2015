# What is the sum of all numbers in the document?

# PART 2
# 

from sys import stdin, argv
from time import time
import re

PART = int(argv[1])

def main():

    doc = JsonDoc()
    # init puzzle
    for line in stdin:
        doc.add(line.rstrip())

    t = time()

    if PART == 1:
        for p in doc.list:
            print("sum:", doc.sumN(p))
    else:
        for p in doc.list:
            removed = doc.removeRed(p)
            print(removed)
            print("sum without red:", doc.sumN(removed))

    print('time:', time()-t)

class JsonDoc():

    def __init__(self):
        self.list = []

    def add(self, doc):
        self.list.append(doc)

    def removeRed(self, doc):
        out_doc = doc
        r = r'{[^{]+:(\"red\")[^}]*'
        result = re.search(r, out_doc)
        while result:
            #print(out_doc)
            start = result.start()
            res2 = re.findall(r'\{', result.group())
            nb_bracket_open = len(res2)
            pos_end = result.end()
            nb_bracket_closed = 0
            while nb_bracket_closed < nb_bracket_open:
                if out_doc[pos_end] == "}":
                    nb_bracket_closed += 1
                pos_end += 1
            tmp_doc = out_doc[0:start]
            tmp_check = out_doc[start:result.span(1)[0]]
            res_check = re.search(r'{[^{]+}', tmp_check)
            if res_check:
                tmp_doc += tmp_check[res_check.end():] + out_doc[result.span(1)[0]:]
            #if re.search(r'"[a-z]+":', tmp_doc):
            #    tmp_doc = out_doc[0:start-4]
            else:
                tmp_doc += out_doc[pos_end:]
            #print(tmp_doc)
            out_doc = tmp_doc
            result = re.search(r, out_doc)

        return out_doc

    def sumN(self, doc):
        docsum = 0
        results = re.findall("(-?\d+)", doc)
        for res in results:
            docsum += int(res)

        return(docsum)


if __name__ == "__main__":
    main()
