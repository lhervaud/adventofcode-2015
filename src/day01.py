# To what floor do the instructions take Santa?

# PART 2
# What is the position of the character that causes Santa to first enter the basement?

from sys import stdin, argv

PART = argv[1]
FLOOR_STOP = -1

def main():

    # init puzzle
    for line in stdin:
        input = line.rstrip()

    if PART == 1:
        print("floor:",run_part1(input))
    else:
        print("position:",run_part2(input))

def run_part1(input):

    floor = 0
    for c in input:
        if c == '(':
            floor +=1
        elif c == ')':
            floor -=1
    return(floor)

def run_part2(input):

    floor = 0
    position = 0
    for c in input:
        position += 1
        if c == '(':
            floor +=1
        elif c == ')':
            floor -=1
        if floor == FLOOR_STOP:
            break
    return(position)

if __name__ == "__main__":
    main()
