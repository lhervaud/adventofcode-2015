# what should his next password be?

# PART 2
# 

from sys import stdin, argv
from time import time
import re

PART = int(argv[1])
PWD_LENGTH = 8

def main():

    pwd = Passwords()
    # init puzzle
    for line in stdin:
        pwd.add(line.rstrip())

    t = time()

    if PART == 1:
        for p in pwd.list:
            print("next:", pwd.find_next(p))
    else:
        for p in pwd.list:
            print("nextx2:", pwd.find_next(pwd.find_next(p)))

    print('time:', time()-t)

class Passwords:

    def __init__(self):
        self.list = []

    def add(self, pwd):
        self.list.append(pwd)

    def find_next(self, pwd):
        pl = list([ord(char) for char in pwd])
        a = ord('a')
        z = ord('z')
        for i0 in range(pl[0], z+1):
            for i1 in range(pl[1], z+1):
                for i2 in range(pl[2], z+1):
                    for i3 in range(pl[3], z+1):
                        for i4 in range(pl[4], z+1):
                            for i5 in range(pl[5], z+1):
                                for i6 in range(pl[6], z+1):
                                    for i7 in range(pl[7], z+1):
                                        pl[7] = pl[7]+1
                                        next = ''.join(chr(char) for char in pl)
                                        if self.valid(next):
                                            return(next)                                        
                                    pl[7] = a-1
                                    pl[6] = pl[6]+1
                                pl[6] = a
                                pl[5] = pl[5]+1
                            pl[5] = a
                            pl[4] = pl[4]+1
                        pl[4] = a
                        pl[3] = pl[3]+1
                    pl[3] = a
                    pl[2] = pl[2]+1
                pl[2] = a
                pl[1] = pl[1]+1
            pl[1] = a
            pl[0] = pl[0]+1


    def valid(self, pwd):
        #print(pwd)
        res = re.search("[iol]", pwd)
        if res:
            return(False)
        else:
            if not re.search("abc|bcd|cde|def|efg|fgh|ghi|hij|ijk|jkl|klm|lmn|mno|nop|opq|pqr|qrs|rst|stu|tuv|uvw|vwx|wxy|xyz", pwd):
                if not res:
                    return(False)
            else:
                res = re.search("(.)\\1.*(.)\\2", pwd)
                if not res:
                    return(False)
        return(True)

if __name__ == "__main__":
    main()
