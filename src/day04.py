# you must find Santa the lowest positive number (no leading zeroes: 1, 2, 3, ...) that produces such a hash

# PART 2
# 

from sys import stdin, argv
import hashlib

PART = int(argv[1])

def main():

    # init puzzle
    for line in stdin:
        input = line.rstrip()

    if PART == 1:
        print("number:",run_part(input, "00000"))
    else:
        print("number:",run_part(input, "000000"))

def run_part(input, start):

    for i in range(10000000):
        hash = hashlib.md5((input+str(i)).encode())
        if hash.hexdigest().startswith(start):
            return(i)

if __name__ == "__main__":
    main()
