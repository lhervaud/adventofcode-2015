# How many strings are nice?

# PART 2
# 

from sys import stdin, argv
import re

PART = int(argv[1])

def main():

    puzzle = []
    # init puzzle
    for line in stdin:
        input = line.rstrip()
        puzzle.append(input)

    if PART == 1:
        print("nb strings:",run_part1(puzzle))
    else:
        print("nb strings:",run_part2(puzzle))

def run_part1(puzzle):

    nb_str_nice = 0
    for input in puzzle:
        valid = True
        # contains at least three vowels
        m = re.findall(r"([aeiou])", input)
        if m:
            if len(m) < 3:
                valid = False
        else:
            valid = False
        # contains at least one letter that appears twice in a row
        if valid:
            m = re.search(r"(.)\1", input)
            if not m:
                valid = False
        # It does not contain the strings ab, cd, pq, or xy
        if valid:
            m = re.search(r"(ab|cd|pq|xy)", input)
            if m:
                valid = False

        if valid:
            nb_str_nice += 1

    return(nb_str_nice)

def run_part2(puzzle):

    nb_str_nice = 0
    for input in puzzle:
        valid = True
        # It contains a pair of any two letters that appears at least twice in the string without overlapping
        m = re.search(r"(..).*\1", input)
        if not m:
            valid = False
        # It contains at least one letter which repeats with exactly one letter between them
        if valid:
            m = re.search(r"(.).\1", input)
            if not m:
                valid = False

        if valid:
            nb_str_nice += 1

    return(nb_str_nice)

if __name__ == "__main__":
    main()
