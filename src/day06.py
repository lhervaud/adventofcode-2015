# how many lights are lit?

# PART 2
# What is the total brightness of all lights combined after following Santa's instructions?

from sys import stdin, argv
import re

PART = int(argv[1])

def main():

    map = Map(1000)
    instructions = Instructions()
    # init puzzle
    for line in stdin:
        input = line.rstrip()
        instructions.add(instruction = input)

    map.apply_instructions(instructions, PART)

    if PART == 1:
        print("nb lights:", map.total())
    else:
        print("total:", map.total())


class Map:

    def __init__(self, size):
        self.map = []
        self.size = size
        for i in range(size):
            self.map.append([0] * size)

    def apply_instructions(self, instructions, part):
        for i in instructions.list:
            for x in range(i.start_x, i.end_x+1):
                for y in range(i.start_y, i.end_y+1):
                    if i.action == "turn on":
                        if part == 1:
                            self.map[y][x] = 1
                        else:
                            self.map[y][x] = self.map[y][x] + 1
                    elif i.action == "turn off":
                        if part == 1:
                            self.map[y][x] = 0
                        else:
                            self.map[y][x] = max(self.map[y][x]-1, 0)
                    elif i.action == "toggle":
                        if part == 1:
                            self.map[y][x] = abs(self.map[y][x]-1)
                        else:
                            self.map[y][x] = self.map[y][x] + 2
                    else:
                        print("unknown action:", i.action)

    def total(self):
        total = 0
        for x in range(self.size):
            for y in range(self.size):
                total += self.map[y][x]
        return(total)

class Instructions:

    def __init__(self):
        self.list = []

    def add(self, instruction):
        result = re.search(r"([a-z\s]+) (\d+),(\d+) through (\d+),(\d+)", instruction)
        if result:
            self.list.append(Instruction(result.group(1), int(result.group(2)), int(result.group(3)), int(result.group(4)), int(result.group(5))))
        else:
            print("cannot add:", instruction)

class Instruction:

    def __init__(self, action, start_x, start_y, end_x, end_y):
        self.action = action
        self.start_x = start_x
        self.start_y = start_y
        self.end_x = end_x
        self.end_y = end_y

if __name__ == "__main__":
    main()
