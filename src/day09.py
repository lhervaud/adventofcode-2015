# What is the distance of the shortest route?

# PART 2
# What is the distance of the longest route?

from sys import stdin, argv
import re

PART = int(argv[1])

def main():

    rt = Routes()
    # init puzzle
    for line in stdin:
        rt.add_distance(line.rstrip())

    rt.compute()
    if PART == 1:
        print("shortest:", rt.get_shortest_distance())
    else:
        print("longest:", rt.get_longest_distance())

class Routes:

    def __init__(self):
        self.distances = []
        self.list = []

    def add_distance(self, line):
        result = re.search(r"([a-zA-Z]+) to ([a-zA-Z]+) = (\d+)", line)
        if result:
            self.distances.append(Distance(result.group(1), result.group(2), int(result.group(3))))
            self.distances.append(Distance(result.group(2), result.group(1), int(result.group(3))))
        else:
            print("cannot add:", line)

    def get_towns(self):
        towns = set()
        for d in self.distances:
            towns.add(d.start)
        return(towns)
    
    def compute(self):
        # init routes
        rt = []
        for d in self.distances:
            rt.append(Route(list([d.start, d.end]), list([d.distance])))

        towns = self.get_towns()
        for i in range(1, len(towns)-1):
            new_rt = []
            for r in rt:
                step = r.steps[-1]
                for d in self.distances:
                    if d.start == step:
                        if d.end not in r.steps:
                            new_rt.append(Route(r.steps + list([d.end]), r.distances + list([d.distance])))
            rt.clear()
            rt = new_rt
        
        self.list = rt
        for r in self.list:
            print(r.steps, r.distances)

    def get_shortest_distance(self):
        d = -1
        for r in self.list:
            s = sum(r.distances)
            if s < d or d == -1:
                d = s
        return(d)

    def get_longest_distance(self):
        d = -1
        for r in self.list:
            s = sum(r.distances)
            if s > d:
                d = s
        return(d)

class Route:

    def __init__(self, steps, distances):
        
        self.steps = steps
        self.distances = distances


class Distance:

    def __init__(self, start, end, distance):
        
        self.start = start
        self.end = end
        self.distance = distance


if __name__ == "__main__":
    main()
