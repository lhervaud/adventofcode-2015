# what is the number of characters of code for string literals minus the number of characters in memory for the values of the strings in total for the entire file?

# PART 2
# total number of characters to represent the newly encoded strings minus the number of characters of code in each original string literal.

from sys import stdin, argv
import re

PART = int(argv[1])

def main():

    sl = SantaList()
    # init puzzle
    for line in stdin:
        sl.add(line.rstrip())

    if PART == 1:
         print("minus:", sl.sum_len_source() - sl.sum_len_in_memroy())
    else:
         print("minus:", sl.sum_len_encoded() - sl.sum_len_source())

class SantaList:

    def __init__(self):
        self.list = []

    def add(self, origin):
        self.list.append(ItemList(origin))

    def sum_len_source(self):
        sum = 0
        for e in self.list:
            sum += len(e.origin)
        return(sum)

    def sum_len_in_memroy(self):
        sum = 0
        for e in self.list:
            sum += len(e.in_memory)
        return(sum)

    def sum_len_encoded(self):
        sum = 0
        for e in self.list:
            sum += len(e.encoded)
        return(sum)

class ItemList:

    def __init__(self, origin):
        self.origin = origin
        self.in_memory = self.to_in_memory(origin)
        self.encoded = self.to_encoded(origin)

    def to_in_memory(self, origin):
        out = origin
        out = re.sub('^"', '', out)
        out = re.sub('"$', '', out)
        out = re.sub('\\\\"', '"', out)
        out = re.sub('\\\\\\\\', '\\\\', out)
        out = re.sub('\\\\x[0-9a-f]{2}', '_', out)
        print(origin,out)
        return(out)

    def to_encoded(self, origin):
        out = origin
        out = re.sub('\\\\', '\\\\\\\\', out)
        out = re.sub('"', '\\\\"', out)
        out = '"' + out + '"'
        print(origin,out)
        return(out)

if __name__ == "__main__":
    main()
