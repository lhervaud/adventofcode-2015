# how many lights are on after 100 steps?

# PART 2
# 

from sys import stdin, argv
from time import time

PART = int(argv[1])
STEPS = 100
OFF = '.'
ON = '#'

def main():

    lights = Lights_Animation()

    # init map
    for line in stdin:
        lights.add_line(line.rstrip())
    
    t = time()

    force_corner = False
    if PART == 2:
        force_corner = True
    print("lights_on :", lights.count_lights_on(lights.animate(STEPS, force_corner)))

    print('time:', time()-t)

class Lights_Animation():

    def __init__(self):
        self.initial_map = []

    def add_line(self, line):
        line_map = []
        for c in line:
            line_map.append(c)
        self.initial_map.append(line_map)

    def force_corner(self, map):
        width = len(map[0])
        height = len(map)
        map[0][0] = ON
        map[0][width-1] = ON
        map[height-1][0] = ON
        map[height-1][width-1] = ON
        return(map)

    def count_neighbors_on(self, map, y, x):
        width = len(map[0])
        height = len(map)
        neighbors = 0
        for r in range(y-1, y+2, 1):
            if r >= 0 and r < height:
                for c in range(x-1, x+2, 1):
                    if c >= 0 and c < width:
                        if r != y or c != x:
                            if map[r][c] == ON:
                                neighbors += 1
        return(neighbors)

    def animate(self, steps, force_corner):
        if force_corner:
            input_map = self.force_corner(self.initial_map)
        else:
            input_map = self.initial_map
        width = len(input_map[0])
        height = len(input_map)
        output_map = []
        for i in range(steps):
            for r in range(0,height):
                new_line = []
                for c in range(0, width):
                    count_on = self.count_neighbors_on(input_map, r, c)
                    # A light which is on stays on when 2 or 3 neighbors are on, and turns off otherwise.
                    # A light which is off turns on if exactly 3 neighbors are on, and stays off otherwise.
                    #print(r, c, count_on)
                    if count_on == 2 or count_on == 3:
                        if input_map[r][c] == ON:
                            new_line.append(ON)
                        else:
                            if input_map[r][c] == OFF and count_on == 3:
                                new_line.append(ON)
                            else:
                                new_line.append(OFF)
                    else:
                        new_line.append(OFF)
                output_map.append(new_line)
            input_map = output_map
            if force_corner:
                input_map = self.force_corner(input_map)
            output_map = []
            #self.print_map(input_map)
            #print("      ")
        return(input_map)

    def count_lights_on(self, map):
        width = len(map[0])
        height = len(map)
        lights_on = 0
        for r in range(height):
            for c in range(width):
                if map[r][c] == ON:
                    lights_on += 1
        return(lights_on)
    
    def print_map(self, map):
        for l in map:
            for c in l:
                print(c, end="")
            print() 


if __name__ == "__main__":
    main()
