# What is the lowest house number of the house to get at least as many presents as the number in your puzzle input?

# PART 2
# 

from sys import stdin, argv
from time import time
import math

PART = int(argv[1])
MAX_PRESENTS = 36000000

def main():

    deliver = Deliver_Present()
    
    t = time()

    print("delivery :", deliver.compute(MAX_PRESENTS))

    print('time:', time()-t)

class Deliver_Present():

    def __init__(self):
        self.prime_list = []
        for i in range(2, 10000):
            if self.is_prime(i):
                self.prime_list.append(i)

    def is_prime(self, n):
        for i in range(2,int(math.sqrt(n))+1):
            if (n%i) == 0:
                return False
        return True

    def compute(self, max_presents):
        dict_computed = {}
        maxdel = 0
        i = 0
        while maxdel <= max_presents:
            i +=1
            maxdel = i * 10
            if self.is_prime(i):
                maxdel += 10
            round = 0
            for j in self.prime_list:
                #if self.is_prime(j):
                if i%j == 0:
                    round += 1
                    if round == 1 :
                        maxdel += dict_computed[int(i/j)]
                        #print(j,dict_computed[int(i/j)])
                    else:
                        if round == 2:
                            maxdel += int(i/j) * 10
                            #print(int(i/j), int(i/j)*10)
                        else:
                            break
                    #print(maxdel)
            dict_computed[i] = maxdel
            if i % 1000 == 0:
                print("House", i, "got" , maxdel, "presents.")
        return(i)


if __name__ == "__main__":
    main()
