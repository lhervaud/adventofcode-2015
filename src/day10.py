# Starting with the digits in your puzzle input, apply this process 40 times. What is the length of the result?

# PART 2
# What is the length of the new result?

from sys import stdin, argv
from time import time
import re
import array

PART = int(argv[1])
PART1_TIMES = 40
PART2_TIMES = 50

def main():

    sq = Sequences()
    # init puzzle
    for line in stdin:
        sq.add(line.rstrip())

    t = time()

    if PART == 1:
        for s in sq.list:
            ln = sq.look_and_say(s, PART1_TIMES)
            #print(s, ln)
            print("length:", len(ln))
    else:
        for s in sq.list:
            ln = sq.look_and_say(s, PART2_TIMES)
            print("length:", len(ln))
    print('time:', time()-t)

class Sequences:

    def __init__(self):
        self.list = []

    def add(self, line):
        self.list.append(line)

    def look_and_say(self, sequence, times):
        print('sequence:', sequence)
        result = list([char for char in sequence])
        for i in range(times):
            print(i)
            prev = ""
            new_result = []
            for c in result:
                if len(prev) == 0:
                    prev = str(c)
                else:
                    if c == prev[-1]:
                        prev = ''.join([prev, c])
                    else:
                        for char in str(len(prev)):
                            new_result.append(char)
                        new_result.append(prev[-1])
                        prev = str(c)
            # last
            if prev:
                for char in str(len(prev)):
                    new_result.append(char)
                new_result.append(prev[-1])
            result = new_result

        return(result)


if __name__ == "__main__":
    main()
